REG := registry.gitlab.com/bat.ggl/reminder-auth:latest

.PHONY: default fmt create_server linter build

default:
	$(info ************ COMMAND NOT SELECT ************)

grpc:
	$(info ************ CREATE GRPC SERVER ************)
	protoc --go_out=plugins=grpc:./internal/interfaces/grpc ./api/v1/auth.proto

lint:
	$(info ************ RUN LINTER ************)
	golangci-lint run -c .golangci.yaml

fmt:
	$(info ************ RUN FROMATING ************)
	go fmt ./...

build:
	$(info ************ BUILD TO ./build/app ************)
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o=./deployments/docker/build/app ./cmd/main.go