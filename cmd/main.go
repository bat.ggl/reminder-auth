package main

import (
	"gitlab.com/bat.ggl/auth/internal/application"
	"gitlab.com/bat.ggl/auth/internal/infrastructure/config"
	"go.uber.org/zap"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	stopping := make(chan os.Signal)
	log, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	sugar := log.Sugar()

	config, err := config.NewConfig()
	if err != nil {
		sugar.Fatal(err)
	}

	app := application.NewApplication(*config, sugar)
	go shutdownMonitor(stopping, app)
	app.Run()
}

func shutdownMonitor(stopping chan os.Signal, app *application.Application) {
	signal.Notify(stopping, os.Interrupt, syscall.SIGTERM)
	for {
		for range stopping {
			app.Shutdown()
		}
	}
}
