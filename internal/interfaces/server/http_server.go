package server

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/bat.ggl/auth/internal/interfaces/grpc"
	v1 "gitlab.com/bat.ggl/auth/internal/interfaces/grpc/api/v1"
	"go.uber.org/zap"
	"net/http"
)

type Servers struct {
	httpPort   int
	grpcPort   int
	app        v1.AuthServer
	grpcServer *grpc.GRPCServer
	httpServer *http.Server
	log        *zap.SugaredLogger
}

func NewServers(httpPort, grpcPort int,
	app v1.AuthServer,
	log *zap.SugaredLogger,
) *Servers {
	return &Servers{
		httpPort: httpPort,
		grpcPort: grpcPort,
		app:      app,
		log:      log,
	}
}

func (s *Servers) Run() error {
	router := chi.NewMux()

	s.grpcServer = grpc.NewGRPCServer(s.grpcPort, s.app)
	s.log.Infof("GRPC server start")

	router.Get("/health", func(writer http.ResponseWriter, request *http.Request) {
		s.writeResponse(struct {
			Msg string `json:"msg"`
		}{Msg: "it will be okay"}, writer, http.StatusOK)
	})

	router.Get("/ready", func(writer http.ResponseWriter, request *http.Request) {
		s.writeResponse(struct {
			Msg string `json:"msg"`
		}{Msg: "i am ready"}, writer, http.StatusOK)
	})

	router.Handle(
		"/metrics",
		promhttp.Handler(),
	)

	server := http.Server{
		Addr:    fmt.Sprintf(":%d", s.httpPort),
		Handler: router,
	}

	go func() {
		err := server.ListenAndServe()
		if err != nil {
			s.log.Fatalf("failed to start http server: %s", err.Error())
		}
	}()

	s.grpcServer.Run()

	return nil
}

func (s *Servers) Shutdown() {
	s.grpcServer.Shutdown()
	if err := s.httpServer.Shutdown(context.Background()); err != nil {
		s.log.Errorf("failed to http server shutdown: %s", err.Error())
	}
}

var (
	errInternalServerError = errors.New("internal server error")
)

func (s *Servers) writeResponse(response interface{}, w http.ResponseWriter, code int) {
	w.Header().Set("content-type", "application-json; charset=utf-8")

	if err, _ := response.(error); err != nil {
		// External users shouldn't see the details of our internal errors
		if code >= http.StatusInternalServerError {
			err = errInternalServerError
		}

		w.Header().Set("X-Result-Error", err.Error())
		w.WriteHeader(code)
		data, _ := json.Marshal(struct {
			Err string `json:"err"`
		}{Err: err.Error()})
		if _, err := w.Write(data); err != nil {
			s.log.Errorf("failed to http error response: %s", err.Error())
		}
	}

	w.WriteHeader(code)
	data, _ := json.Marshal(struct {
		Data interface{} `json:"data"`
	}{Data: response})
	if _, err := w.Write(data); err != nil {
		s.log.Errorf("failed to http response: %s", err.Error())
	}
}
