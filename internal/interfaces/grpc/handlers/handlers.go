package handlers

import (
	"context"
	request2 "gitlab.com/bat.ggl/auth/internal/adapters/request"
	"gitlab.com/bat.ggl/auth/internal/domain"
	"gitlab.com/bat.ggl/auth/internal/infrastructure/metrics"
	v1 "gitlab.com/bat.ggl/auth/internal/interfaces/grpc/api/v1"
	"gitlab.com/bat.ggl/auth/internal/usecases"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Container struct {
	Users   *usecases.Users
	Metrics *metrics.MetricsContainer
}

func (a *Container) Login(ctx context.Context, req *v1.LoginRequest) (*v1.LoginResponse, error) {
	response := &v1.LoginResponse{}
	var err error

	login := request2.Login{
		Login:    req.Username,
		Password: req.Password,
	}

	response.Token, err = a.Users.Login(login)
	if err != nil {
		if _, ok := err.(*domain.ErrorValidation); ok {
			return response, status.Error(codes.InvalidArgument, err.Error())
		}

		switch err {
		case domain.ErrServerError:
			return response, status.Error(codes.Internal, domain.ErrServerError.Error())
		case domain.ErrNotExists:
			return response, status.Error(codes.NotFound, err.Error())
		}

		return response, status.Error(codes.Internal, domain.ErrServerError.Error())
	}

	return response, nil
}

func (a *Container) Register(ctx context.Context, request *v1.RegisterRequest) (*v1.RegisterResponse, error) {
	response := &v1.RegisterResponse{Status: false}

	var err error
	register := request2.Register{
		Login:    request.Username,
		Password: request.Password,
		Email:    request.Email,
	}

	response.Status, err = a.Users.Create(register)
	if err != nil {
		if _, ok := err.(*domain.ErrorValidation); ok {
			return response, status.Error(codes.InvalidArgument, err.Error())
		}

		switch err {
		case domain.ErrServerError:
			return response, status.Error(codes.Internal, domain.ErrServerError.Error())
		case domain.ErrExists:
			return response, status.Error(codes.AlreadyExists, err.Error())
		}

		return response, status.Error(codes.Internal, domain.ErrServerError.Error())
	}

	return response, nil
}

func (a *Container) RestorePassword(ctx context.Context, restore *v1.RestoreRequest) (*v1.RestoreResponse, error) {
	response := &v1.RestoreResponse{Status: false}
	var err error

	email := request2.Restore{Email: restore.Email}

	response.Status, err = a.Users.Restore(email)
	if err != nil {
		if _, ok := err.(*domain.ErrorValidation); ok {
			return response, status.Error(codes.InvalidArgument, err.Error())
		}

		switch err {
		case domain.ErrNotExists:
			return response, status.Error(codes.NotFound, domain.ErrNotExists.Error())
		case domain.ErrServerError:
			return response, status.Error(codes.Internal, domain.ErrServerError.Error())
		}

		return response, status.Error(codes.Internal, domain.ErrServerError.Error())
	}

	return response, nil
}

func (a *Container) CheckToken(ctx context.Context, check *v1.CheckRequest) (*v1.CheckResponse, error) {
	response := &v1.CheckResponse{Status: false}
	var err error
	response.Status, err = a.Users.CheckJWTToken(check.Token)
	if err != nil {
		if _, ok := err.(*domain.ErrorValidation); ok {
			return response, status.Error(codes.InvalidArgument, err.Error())
		}

		switch err {
		case domain.ErrInvalidToken:
			return response, status.Error(codes.InvalidArgument, err.Error())
		case domain.ErrServerError:
			return response, status.Error(codes.Internal, domain.ErrServerError.Error())
		}

		return response, status.Error(codes.Internal, domain.ErrServerError.Error())
	}

	return response, nil
}
