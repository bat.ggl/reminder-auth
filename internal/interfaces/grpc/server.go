package grpc

import (
	"fmt"
	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	v1 "gitlab.com/bat.ggl/auth/internal/interfaces/grpc/api/v1"
	"google.golang.org/grpc"
	"net"
)

type GRPCServer struct {
	port   int
	app    v1.AuthServer
	Server *grpc.Server
}

func NewGRPCServer(port int, app v1.AuthServer) *GRPCServer {
	server := &GRPCServer{
		port: port,
		app:  app,
	}
	return server
}

func (s *GRPCServer) Run() {
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", s.port))
	if err != nil {
		panic(err)
	}

	grpcMetrics := grpc_prometheus.NewServerMetrics()

	s.Server = grpc.NewServer(
		grpc.StreamInterceptor(grpcMetrics.StreamServerInterceptor()),
		grpc.UnaryInterceptor(grpcMetrics.UnaryServerInterceptor()),
	)

	grpcMetrics.InitializeMetrics(s.Server)

	v1.RegisterAuthServer(s.Server, s.app)
	if err := s.Server.Serve(listener); err != nil {
		panic(err)
	}
}

func (s *GRPCServer) Shutdown() {
	s.Server.GracefulStop()
}
