package usecases

import (
	"gitlab.com/bat.ggl/auth/internal/adapters/request"
	"gitlab.com/bat.ggl/auth/internal/domain"
	"gitlab.com/bat.ggl/auth/internal/domain/entity"
	"time"
)

func (u *Users) Create(register request.Register) (bool, error) {
	err := register.Validate()
	if err != nil {
		return false, domain.NewErrValidation(err.Error())
	}

	foundUser, err := u.userRepo.GetUserByEmail(register.Email)
	if err != nil {
		u.logger.Errorf("%v: %s", domain.ErrServerError, err)
		return false, domain.ErrServerError
	}

	if foundUser != nil {
		return false, domain.ErrExists
	}

	user := entity.User{
		Login:     register.Email,
		Password:  u.createHashSha256FromPassword(register.Password),
		Email:     register.Email,
		IsActive:  0,
		Social:    "{}",
		Gender:    "",
		CreatedAt: time.Now().UTC(),
		UpdatedAt: time.Now().UTC(),
	}

	_, err = u.userRepo.Create(&user)
	if err != nil {
		u.logger.Errorf("error request db: %s, %#v", err.Error(), user)
		return false, domain.ErrServerError
	}

	return true, nil
}
