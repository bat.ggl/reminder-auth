package usecases

import (
	"crypto/sha256"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/bat.ggl/auth/internal/domain/entity"
	"gitlab.com/bat.ggl/auth/internal/domain/repostitory"
	"go.uber.org/zap"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

var (
	secretSigned = "todo"
)


type Users struct {
	userRepo repostitory.UserRepository
	logger   *zap.SugaredLogger
}

func NewUserUsecase(userRepo repostitory.UserRepository, logger *zap.SugaredLogger) *Users {
	return &Users{
		userRepo: userRepo,
		logger:   logger,
	}
}

func (u *Users) createJWTToken(user *entity.User) (string, error) {
	mapClaims := jwt.MapClaims{}
	mapClaims["username"] = user.Login
	mapClaims["userID"] = user.Id
	mapClaims["expired"] = time.Now().Add(time.Hour * 72)

	claim := jwt.NewWithClaims(jwt.SigningMethodHS256, mapClaims)
	token, err := claim.SignedString([]byte(secretSigned))
	if err != nil {
		return "", err
	}

	return token, nil
}

func (u *Users) generatePassword(length int) string {
	rand.Seed(time.Now().UnixNano())
	alphabet := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*()")
	var b strings.Builder
	for i := 0; i < length; i++ {
		b.WriteRune(alphabet[rand.Intn(len(alphabet))])
	}

	return u.createHashSha256FromPassword(b.String())
}

func (u *Users) createHashSha256FromPassword(pwd string) string {
	var data [32]byte = sha256.Sum256([]byte(pwd))
	hash := fmt.Sprintf("%x", data)

	return hash
}

func (u *Users) parseJWTToken(hash string) (*jwt.Token, error) {
	token, err := jwt.Parse(hash, func(tkn *jwt.Token) (interface{}, error) {
		if _, ok := tkn.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", tkn.Header["alg"])
		}

		return []byte(secretSigned), nil
	})

	if err != nil {
		return nil, err
	}

	return token, err
}

func getDataFromJwtClaims(claims jwt.MapClaims) (uint64, error) {
	userID, err := strconv.ParseUint(fmt.Sprintf("%.f", claims["userID"]), 10, 64)
	if err != nil {
		return 0, err
	}

	return userID, nil
}
