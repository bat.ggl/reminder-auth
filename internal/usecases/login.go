package usecases

import (
	"gitlab.com/bat.ggl/auth/internal/adapters/request"
	"gitlab.com/bat.ggl/auth/internal/domain"
)

func (u *Users) Login(login request.Login) (string, error) {
	if err := login.Validate(); err != nil {
		return "nil", domain.NewErrValidation(err.Error())
	}

	user, err := u.userRepo.GetUserByLoginPassword(
		login.Login,
		u.createHashSha256FromPassword(login.Password))
	if err != nil {
		return "", domain.ErrServerError
	}

	if user == nil {
		return "", domain.ErrNotExists
	}

	token, err := u.createJWTToken(user)
	if err != nil {
		u.logger.Errorf("%v: %s", domain.ErrServerError, err)
		return "", domain.ErrServerError
	}

	return token, err
}
