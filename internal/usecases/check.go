package usecases

import (
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/bat.ggl/auth/internal/domain"
)

func (u *Users) CheckJWTToken(hash string) (bool, error) {
	token, err := u.parseJWTToken(hash)
	if err != nil {
		if _, ok := err.(*jwt.ValidationError); ok {
			return false, domain.NewErrValidation("Token hash is not valid")
		}
		return false, domain.ErrServerError
	}

	if _, ok := token.Claims.(jwt.Claims); !ok && !token.Valid {
		return false, nil
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return false, nil
	}

	if !ok && !token.Valid {
		return false, domain.ErrInvalidToken
	}

	userID, err := getDataFromJwtClaims(claims)
	if err != nil {
		return false, domain.ErrServerError
	}

	user, err := u.userRepo.GetUserByID(userID)
	if err != nil {
		u.logger.Errorf("%v: %s", domain.ErrServerError, err)
		return false, domain.ErrServerError
	}

	if user == nil {
		return false, nil
	}

	return true, nil
}
