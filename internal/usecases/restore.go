package usecases

import (
	"gitlab.com/bat.ggl/auth/internal/adapters/request"
	"gitlab.com/bat.ggl/auth/internal/domain"
	"gitlab.com/bat.ggl/auth/internal/domain/entity"
)

func (u *Users) Restore(restore request.Restore) (bool, error) {
	err := restore.Validate()
	if err != nil {
		return false, domain.NewErrValidation(err.Error())
	}

	foundUser, err := u.userRepo.GetUserByEmail(restore.Email)
	if err != nil {
		u.logger.Errorf("%v: %s", domain.ErrServerError, err)
		return false, domain.ErrServerError
	}

	if foundUser == nil {
		return false, domain.ErrNotExists
	}

	foundUser.Password = u.generatePassword(entity.MinLengthOfPassword)
	_, err = u.userRepo.Update(foundUser)
	if err != nil {
		u.logger.Errorf("%v: %s", domain.ErrServerError, err)
		return false, domain.ErrServerError
	}

	return true, nil
}
