package application

import (
	"github.com/go-pg/pg"
	"gitlab.com/bat.ggl/auth/internal/adapters/repository/postgres"
	"gitlab.com/bat.ggl/auth/internal/infrastructure/config"
	"gitlab.com/bat.ggl/auth/internal/infrastructure/datasource"
	"gitlab.com/bat.ggl/auth/internal/infrastructure/metrics"
	"gitlab.com/bat.ggl/auth/internal/interfaces/grpc/handlers"
	"gitlab.com/bat.ggl/auth/internal/interfaces/server"
	"gitlab.com/bat.ggl/auth/internal/usecases"
	"go.uber.org/zap"
)

type Application struct {
	config config.Config
	logger *zap.SugaredLogger
	db     *pg.DB
	server *server.Servers
}

func NewApplication(config config.Config, logger *zap.SugaredLogger) *Application {
	return &Application{
		config: config,
		logger: logger,
	}
}

func (a *Application) Run() {
	var err error
	a.db, err = datasource.NewPostgres(a.logger, a.config.PostgresDsn, a.config.IsProd)
	if err != nil {
		a.logger.Fatal("failed to db connect: %s", err.Error())
	}

	a.logger.Info("db connection established")
	mtr := metrics.NewMetricsContainer()

	userRepo := postgres.NewUserRepository(a.db, mtr)
	users := usecases.NewUserUsecase(userRepo, a.logger)

	app := &handlers.Container{
		Users:   users,
		Metrics: mtr,
	}

	a.server = server.NewServers(a.config.ServerAddr, a.config.ServerGRPCAddr, app, a.logger)
	if err := a.server.Run(); err != nil {
		panic(err)
	}
}

func (a *Application) Shutdown() {
	a.db.Close()
	a.server.Shutdown()
}
