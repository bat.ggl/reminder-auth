package config

import (
	"github.com/spf13/viper"
)

type Config struct {
	IsProd         bool   `mapstructure:"is_prod"`
	ServerAddr     int    `mapstructure:"server_addr"`
	ServerGRPCAddr int    `mapstructure:"grpc_addr"`
	PostgresDsn    string `mapstructure:"postgres_dsn"`
}

func NewConfig() (*Config, error) {
	config := &Config{}
	viper.AutomaticEnv()
	viper.SetDefault("is_prod", true)
	viper.SetDefault("http_server", 8080)
	viper.SetDefault("grpc_server", 5051)
	viper.SetDefault("postgres_dsn", "postgresql://")
	if err := viper.Unmarshal(config); err != nil {
		return nil, err
	}

	return config, nil
}
