package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
)

/***
* логируем, время sql
* ошибки (критические)
* мониторим память и CPU
* время ответа сервиса
* статусы кодов ответа
 */

type MetricsContainer struct {
	SqlQueryLatency  *prometheus.HistogramVec
	HttpQueryLatency *prometheus.HistogramVec
}

func NewMetricsContainer() *MetricsContainer {
	container := &MetricsContainer{}

	container.HttpQueryLatency = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: "http_request_duration_seconds",
		Help: "Duration of all HTTP request",
	}, []string{"code", "handler", "method"})

	container.SqlQueryLatency = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: "sql_time_query_execute_seconds",
		Help: "time of execute SQL",
	}, []string{"name", "tableName"})

	prometheus.MustRegister(
		container.HttpQueryLatency,
		container.SqlQueryLatency,
	)

	return container
}

func (m *MetricsContainer) SQLLatency(queryLabel, tableName string, elapsed float64) {
	m.SqlQueryLatency.WithLabelValues(queryLabel, tableName).Observe(elapsed)
}

func (m *MetricsContainer) HTTPQueryLatency(code, handler, method string, elapsed float64) {
	m.HttpQueryLatency.WithLabelValues(code, handler, method).Observe(elapsed)
}
