package mock

type PromMock struct{}

func (m *PromMock) SQLLatency(queryLabel, tableName string, elapsed float64) {}

func (m *PromMock) HTTPQueryLatency(code, handler, method string, elapsed float64) {}
