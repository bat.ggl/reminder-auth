package datasource

import (
	"github.com/go-pg/pg"
	"go.uber.org/zap"
)

func NewPostgres(logger *zap.SugaredLogger, dsn string, prod bool) (*pg.DB, error) {
	options, err := pg.ParseURL(dsn)
	if err != nil {
		return nil, err
	}

	conn := pg.Connect(options)
	if !prod {
		conn.AddQueryHook(dbLogger{logger: logger})
	}

	return pg.Connect(options), nil
}

type dbLogger struct {
	logger *zap.SugaredLogger
}

func (d dbLogger) BeforeQuery(event *pg.QueryEvent) {}

func (d dbLogger) AfterQuery(event *pg.QueryEvent) {
	sql, err := event.FormattedQuery()
	if err != nil {
		d.logger.Error("SQL err: ", err.Error())
	} else {
		d.logger.Infof("SQL: ", sql)
	}
}
