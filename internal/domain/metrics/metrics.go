package metrics

type Metrics interface {
    SQLLatency(queryLabel, tableName string, elapsed float64)
	HTTPQueryLatency(code, handler, method string, elapsed float64)
}