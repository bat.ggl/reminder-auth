package entity

import "time"

const MinLengthOfPassword = 8

/**
user structure.
*/
type User struct {
	table     struct{} `pg:"users:alias:u"` // nolint
	Id        int
	Login     string
	Password  string
	Email     string
	IsActive  int
	Social    string
	Gender    string
	CreatedAt time.Time
	UpdatedAt time.Time
}
