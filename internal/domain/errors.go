package domain

import (
	"github.com/go-errors/errors"
)

var (
	ErrServerError  = errors.New("internal server error")
	ErrNotExists    = errors.New("Not exists")
	ErrExists       = errors.New("Exists")
	ErrInvalidToken = errors.New("Token invalid")
)

type ErrorValidation struct {
	s string
}

func NewErrValidation(message string) *ErrorValidation {
	return &ErrorValidation{
		s: message,
	}
}
func (e *ErrorValidation) Error() string {
	return e.s
}
