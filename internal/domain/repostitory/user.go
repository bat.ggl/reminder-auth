package repostitory

import "gitlab.com/bat.ggl/auth/internal/domain/entity"

type UserRepository interface {
	Create(entity *entity.User) (*entity.User, error)
	Update(user *entity.User) (*entity.User, error)
	GetUserByLoginPassword(name, password string) (*entity.User, error)
	GetUserByEmail(email string) (*entity.User, error)
	GetUserByID(id uint64) (*entity.User, error)
}
