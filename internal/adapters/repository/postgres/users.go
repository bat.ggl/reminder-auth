package postgres

import (
	"github.com/go-errors/errors"
	"github.com/go-pg/pg"
	"gitlab.com/bat.ggl/auth/internal/domain/entity"
	"gitlab.com/bat.ggl/auth/internal/domain/metrics"
	"time"
)

type UserRepository struct {
	*pg.DB
	metrics.Metrics
}

func NewUserRepository(db *pg.DB, metrics metrics.Metrics) *UserRepository {
	return &UserRepository{db, metrics}
}

func (u *UserRepository) Create(entity *entity.User) (*entity.User, error) {
	start := time.Now()
	_, err := u.DB.Model(entity).Insert()
	u.Metrics.SQLLatency("CreateUser", "users", time.Since(start).Seconds())
	if err != nil {
		return nil, errors.Errorf("failed to create user: %w", err)
	}

	return entity, nil
}

func (u *UserRepository) Update(user *entity.User) (*entity.User, error) {
	start := time.Now()
	_, err := u.DB.Model(user).Where("id =? ", user.Id).Update()
	u.Metrics.SQLLatency("UpdateUser", "users", time.Since(start).Seconds())
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (u *UserRepository) GetUserByLoginPassword(name, password string) (*entity.User, error) {
	start := time.Now()
	user := &entity.User{}
	err := u.DB.Model(user).
		Where("login =? and password =?", name, password).
		Select()
	u.Metrics.SQLLatency("GetUserByLoginPassword", "users", time.Since(start).Seconds())
	if err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, nil
		}

		return nil, err
	}

	return user, nil
}

func (u *UserRepository) GetUserByEmail(email string) (*entity.User, error) {
	user := entity.User{}

	start := time.Now()
	err := u.DB.Model(&user).Where("email = ?", email).Select()
	u.Metrics.SQLLatency("GetUserByEmail", "users", time.Since(start).Seconds())
	if err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, nil
		}

		return nil, err
	}

	return &user, nil
}

func (u *UserRepository) GetUserByID(id uint64) (*entity.User, error) {
	start := time.Now()
	user := entity.User{}
	err := u.DB.Model(&user).Where("id = ?", id).Select()
	u.Metrics.SQLLatency("GetUserByID", "users", time.Since(start).Seconds())

	if err != nil {
		if errors.Is(err, pg.ErrNoRows) {
			return nil, nil
		}

		return nil, err
	}

	return &user, nil
}
