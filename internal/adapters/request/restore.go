package request

import (
	"gitlab.com/bat.ggl/auth/internal/adapters/request/validator"
)

type Restore struct {
	Email string `validate:"required,email"`
}

func (r *Restore) Validate() error {
	return validator.Validate(r)
}
