package request

import (
	"gitlab.com/bat.ggl/auth/internal/adapters/request/validator"
)

type Login struct {
	Login    string `validate:"required,min=3,max=30"`
	Password string `validate:"required,min=8,max=60"`
}

func (l *Login) Validate() error {
	return validator.Validate(l)
}
