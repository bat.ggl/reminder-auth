package request

import (
	"gitlab.com/bat.ggl/auth/internal/adapters/request/validator"
)

type Register struct {
	Login    string `validate:"required,min=3,max=30"`
	Password string `validate:"required,min=8,max=60"`
	Email    string `validate:"required,email"`
}

func (r *Register) Validate() error {
	return validator.Validate(r)
}
