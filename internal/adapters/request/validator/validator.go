package validator

import (
	"github.com/go-errors/errors"
	"github.com/go-playground/validator/v10"
)

func Validate(object interface{}) error {
	validate := validator.New()
	err := validate.Struct(object)
	if err != nil {
		if _, ok := err.(*validator.InvalidValidationError); ok {
			return errors.Errorf("value: %s", err)
		}

		for _, err := range err.(validator.ValidationErrors) {
			return errors.Errorf("Field: %s, value: %s", err.ActualTag(), err.Value())
		}
	}

	return nil
}
