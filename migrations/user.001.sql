create table if not exists users
(
	id serial not null,
	login text not null,
	password text not null,
	email text not null,
	is_active integer default 0,
	social jsonb not null,
	sex integer,
	created_at timestamp not null,
	updated_at timestamp
);

alter table users owner to postgres;

create unique index if not exists users_email_uindex
	on users (email);

create unique index if not exists users_name_uindex
	on users (login);

create index if not exists users_id_index
	on users (id);
