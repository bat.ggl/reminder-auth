module gitlab.com/bat.ggl/auth

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-errors/errors v1.1.1
	github.com/go-pg/pg v8.0.7+incompatible
	github.com/go-playground/validator/v10 v10.3.0
	github.com/golang/protobuf v1.4.2
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/onsi/ginkgo v1.14.1 // indirect
	github.com/onsi/gomega v1.10.2 // indirect
	github.com/prometheus/client_golang v0.9.3
	github.com/sirupsen/logrus v1.2.0 // indirect
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.5.1 // indirect
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9 // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/grpc v1.31.1
	google.golang.org/protobuf v1.25.0
	mellium.im/sasl v0.2.1 // indirect
)
